#!./env/bin/python

from tkinter import Tk, BOTH, END, filedialog, StringVar, messagebox
from tkinter.ttk import Frame, Button, Style, Entry, Progressbar, Label

class MainFrame(Frame):

    def __init__(self):
        super().__init__()

        self.init_ui()
        self.set_margin()

    # def doc_browse_click(self):
    #     file = filedialog.askopenfilename(initialdir = ".", title = "Select A File", filetype =
    #     (("docx files","*.docx"), ))
    #     messagebox.showinfo('Information', 'haha')

    def browse_click(self, entry, type):
        file_type = "%s files" % type
        file_ext = "*.%s" % type
        file = filedialog.askopenfilename(initialdir = ".", title = "Select A File", filetype =
        ((file_type, file_ext), ))
        entry.delete(0, END)
        entry.insert(0, file)
        entry.configure(state='readonly')

    def set_margin(self):
        for child in self.winfo_children():
            child.grid_configure(padx=3, pady=10)

    def spellcheck(self):
        doc_f = self.doc_file.get()
        dict_f = self.dict_file.get()
        messagebox.showinfo('info', doc_f + ' ' + dict_f)
        # self.progressbar.start()

    def init_ui(self):
        self.style = Style()
        self.style.theme_use("vista")
        self.grid(padx=5, pady=5)
        # self.style.configure("TButton", padding=(0, 5, 0, 5), margin=(10,10,10,10), font='serif 10')

        self.master.title("PySpellcheck")
        self.pack(fill=BOTH, expand=1)

        self.doc_lbl = Label(self, text="Document: ")
        self.doc_lbl.grid(row=0)
        self.doc_file = StringVar()
        self.doc_entry = Entry(self,width=30, textvariable=self.doc_file)
        self.doc_entry.grid(row=0,column=1)
        self.doc_browse_btn = Button(self, text="Browse",
            command=lambda: self.browse_click(self.doc_entry, "docx"))
        self.doc_browse_btn.grid(row=0,column=2)

        self.dict_lbl = Label(self, text="Dictionary: ")
        self.dict_lbl.grid(row=1)
        self.dict_file = StringVar()
        self.dict_entry = Entry(self,width=30, textvariable=self.dict_file)
        self.dict_entry.grid(row=1,column=1)
        self.dict_browse_btn = Button(self, text="Browse",
            command=lambda: self.browse_click(self.dict_entry, "txt"))
        self.dict_browse_btn.grid(row=1,column=2)

        self.progressbar = Progressbar(self, length=400)
        self.progressbar.grid(row=2, columnspan=3)
        self.start_btn = Button(self, text="Start Spellcheck", command=self.spellcheck)
        self.start_btn.grid(row=3, columnspan=3)

def main():
    root = Tk()
    root.resizable(False, False)
    app = MainFrame()
    root.mainloop()

if __name__ == '__main__':
    main()
