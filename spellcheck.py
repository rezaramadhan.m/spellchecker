#!./env/bin/python
import sys
import docx
import copy
import apply_format
import re


def read_dict():
    word_list = []
    with open(DICT_FNAME, 'r') as f_dict:
        word_list = [line.rstrip() for line in f_dict]

    return set(word_list)

def mark_spelling_error(run):
    run.font.color.rgb = docx.shared.RGBColor(0xff, 0x00, 0x00)

def strip_nonalpha(word):
    s = re.sub(r'^\W+', '', word)
    return re.sub(r'\W+$', '', s)

def process_paragraphs(paragraphs, w_list):
    format_func = lambda x:x.font.__setattr__('highlight_color', docx.enum.text.WD_COLOR_INDEX.RED)

    for paragraph in paragraphs:
        # print('"', end='')
        # print(paragraph.runs)
        misspell_idx = []
        txt = paragraph.text
        prev_word = ''
        cumulative_pos = 0
        for word in txt.split(' '):
            word = strip_nonalpha(word)
            if (word.isalpha() and not word.lower() in w_list):
                print(word)
                pos = txt.find(word)
                misspell_idx.append((cumulative_pos + pos, len(word)))
                txt = txt[pos:]
                cumulative_pos += pos

            prev_word = word
        print(misspell_idx)

        for idx in misspell_idx:
            start = idx[0]
            length = idx[1]
            apply_format.apply_format_to_range(paragraph, start, start + length, format_func)

def process_tables(tables, w_list):
    i = 0
    for tab in tables:
        for row in tab.rows:
            for cell in row.cells:
                # i+=1
                # print('___________________________',i)
                # print([p.text for p in cell.paragraphs])
                process_paragraphs(cell.paragraphs, w_list)
                process_tables(cell.tables, w_list)

def main(doc_fname = 'docx/long01.docx', dict_fname = 'dict/ID_dict'):
    # Read dictionary, put word list to set
    w_list = read_dict()

    # Read document, iterate paragraph & table
    doc = docx.Document(doc_fname)
    process_paragraphs(doc.paragraphs, w_list)
    process_tables(doc.tables, w_list)
    doc.save('result.docx')


if __name__ == '__main__':
    main()
